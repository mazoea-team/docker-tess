# How to build the image

Go to `te-external-tesseract4/build-scripts` and execute:
```
docker.build.training.bat

docker login registry.gitlab.com
docker push registry.gitlab.com/mazoea-team/docker-tess
```

The image is not built using files from this repository because it relies on a much bigger project namely `te-external-tesseract`.